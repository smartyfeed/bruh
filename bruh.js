
var fs = require('fs');


var input = fs.readFileSync('input.txt', 'UTF8').split('\n');
var gamma = '';
var epsilon = '';

for(i = 0; i < input[0].length; i++) {
    var zeroCount = 0;
    var onesCount = 0;
    for(j = 0; j < input.length; j++) {
        var number = input[j];
        var char = number[i];
        if(char == 0) {
            zeroCount++;
        } else {
            onesCount++;
        }

    }
    if(zeroCount > onesCount) {
        gamma += 0;
        epsilon += 1;
    } else {
        gamma += 1;
        epsilon += 0;
    }

}

var gammaDec = parseInt(gamma, 2);
var epsilonDec = parseInt(epsilon, 2);

var output = gammaDec * epsilonDec;

console.log(output);
